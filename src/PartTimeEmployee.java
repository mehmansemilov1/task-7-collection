class PartTimeEmployee extends Employee {
    private int hoursWorked;
    private double hourlySalary;

    public PartTimeEmployee(String name, int experience, int hoursWorked, double hourlySalary) {
        super(name, experience);
        this.hoursWorked = hoursWorked;
        this.hourlySalary = hourlySalary;
    }

    public double getHoursSalary() {
        return hoursWorked * hourlySalary;
    }

    @Override
    public String toString() {
        return "PartTimeEmployee{" +
                "ad='" + super.name + '\'' +
                ", tecrube=" + super.getExperience() +
                ", islenenSaat=" + hoursWorked +
                ", saatliqMaas=" + hourlySalary +
                '}';
    }
}