 class Employee {
        String name;
        private int experience;

        public Employee(String name, int experience) {
            this.name = name;
            this.experience = experience;
        }

        public int getExperience() {
            return experience;
        }

        @Override
        public String toString() {
            return "Employee{" +
                    "ad='" + name + '\'' +
                    ", tecrube=" + experience +
                    '}';
        }
    }

