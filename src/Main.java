import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new FullTimeEmployee("Mehman", 3, 50000));
        employees.add(new FullTimeEmployee("Ali", 1, 60000));
        employees.add(new PartTimeEmployee("Vali", 4, 20, 25));
        employees.add(new PartTimeEmployee("Nihad", 2, 15, 20));

        List<Employee> filteredEmployees = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getExperience() > 2) {
                filteredEmployees.add(employee);
            }
        }

        filteredEmployees.sort((e1, e2) -> Integer.compare(e2.getExperience(), e1.getExperience()));

        Map<String, Double> salaryMap = new HashMap<>();
        if (!filteredEmployees.isEmpty()) {
            Employee maxSalaryEmployee = filteredEmployees.get(0);
            Employee minSalaryEmployee = filteredEmployees.get(filteredEmployees.size() - 1);
            if (maxSalaryEmployee instanceof FullTimeEmployee) {
                salaryMap.put("maxEmekhaqqi", ((FullTimeEmployee) maxSalaryEmployee).getSalary());
            } else if (maxSalaryEmployee instanceof PartTimeEmployee) {
                salaryMap.put("maxEmekhaqqi", ((PartTimeEmployee) maxSalaryEmployee).getHoursSalary());
            }

            if (minSalaryEmployee instanceof FullTimeEmployee) {
                salaryMap.put("maxEmekhaqqi", ((FullTimeEmployee) minSalaryEmployee).getSalary());
            } else if (minSalaryEmployee instanceof PartTimeEmployee) {
                salaryMap.put("maxEmekhaqqi", ((PartTimeEmployee) minSalaryEmployee).getHoursSalary());
            }
        }

        System.out.println("2-dən çox təcrübesi olan işçiler ");
        for (Employee employee : filteredEmployees) {
            System.out.println(employee);
        }

        System.out.println("Emek haqqi xeritesi :  " + salaryMap);
    }
}